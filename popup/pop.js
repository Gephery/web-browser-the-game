/**
 * Created by gephery on 9/16/17.
 */
(function() {
    "use strict";

    // Add Event listeners 
    function addSendState(el) {
      el.addEventListener("click", sendState);
    }
    addSendState(document.getElementById("gameonbutt"));
    [...document.getElementsByName("click-type")].forEach(addSendState);

    browser.runtime.getBackgroundPage().then(setParPop, handErrIPop);

    /**
     * Sends current tab and the background the state of the on button.
     * @param el {HTMLElement} The on button.
     */
    function sendState(el) {
        // Send info to tabs
        browser.tabs.query({
            currentWindow: true,
            active: true
        }).then(_sendState, handErrIPop);

        // Send into to top side
        let state = document.getElementById("gameonbutt").checked;
        let ct1 = document.getElementById("ct1");
        let ct2 = document.getElementById("ct2");
        let ct3 = document.getElementById("ct3");
        let clickType = ct1.checked ? ct1.value : ct2.checked ? ct2.value : ct3.value;
        browser.runtime.sendMessage({"stateinfo": true, "state": state, "ctinfo": true, "ct": clickType});
    }

    /**
     * "private" function used by sendState for sending on state to current tab.
     * @param tabs
     * @private
     */
    function _sendState(tabs) {
        let state = document.getElementById("gameonbutt").checked;
        let ct1 = document.getElementById("ct1");
        let ct2 = document.getElementById("ct2");
        let ct3 = document.getElementById("ct3");
        let clickType = ct1.checked ? ct1.value : ct2.checked ? ct2.value : ct3.value;
        for (let tab of tabs) {
            if (tab != null) {
                browser.tabs.sendMessage(
                    tab.id,
                    {"stateinfo": true, "state": state, "ctinfo": true, "ct": clickType}
                );
            }
        }
    }

    /**
     * Sets the on button state on load of pop up.
     * @param page
     */
    function setParPop(page) {
        console.log("Set pop state to " + page.Notter.on + "\n");
        document.getElementById("gameonbutt").checked = page.Notter.on;
        document.getElementById("ct1").checked = page.Notter.ct == "block";
        document.getElementById("ct2").checked = page.Notter.ct == "hole";
        document.getElementById("ct3").checked = page.Notter.ct == "hold";
    }

    /**
     * General error handling function.
     * @param err
     */
    function handErrIPop(err) {
        console.log("!!Err!! " + err + "\n");
    }
})();
