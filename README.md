# Wed Browser The Game (WBTG)  
A Firefox extension game.  
 
## Purpose  
Education tool for learning HTML and Javascript. Unlike 
normal educational tools, WBTG focuses on making an enjoyable 
game before education. This is akin to the approach Minecraft 
took, but WBTG grows off what Minecraft did wrong. Namely Minecraft 
still has yet to release a first party modding API. First party APIs 
for games are few and far between. The lack of prevalence is stifling 
the education space and future creators. Truly the key to a productive 
programmer is the dedication. WBTG is positioned to do better than 
code.org’s primitive attempts to create something enjoyable.  

## Idea Technical  
Think of a webpage’s HTML as the level editor, JS as the API, and the 
user as the learner. Using `div`s to create things like surfaces, 
paths, objectives (e.g. coins), UI, really any part of the game experience. 
Using HTML and JS to craft a game engine without the priority languages. 
The JS functions similar to how Bukkit/Spigot API functions with overriding 
events.  Ideally the page on the main website instructs people how to setup 
a basic web server on their own computer/old computer. Another minor goal 
is to encourage more people to host their own data. Granted their may be 
a main page on the WBTG website to showcase some excellent user creations 
it will be mostly freerange.  

### How to test & use for now...  
Currently it is a firefox extension only and a demo at that. To start debugging 
and working on your own version do this:
1. Navigate to `about:debugging#addons` in the url.
1. Press `Load Temporary Add-on...` and click the `manifest.json`. 
1. Any time you need to try new code press `reload` on the `about:debugging#addons` page. 
   Popup HTML/CSS can be changed without reloading the page. 
1. Reload the page. Pages like `google.com` work well for testing. 

# Terms
You are free to use this code, just reference me. 
